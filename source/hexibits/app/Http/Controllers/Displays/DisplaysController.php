<?php

namespace App\Http\Controllers\Displays;

use App\Http\Controllers\Controller;
use App\Model\Display;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class DisplaysController extends Controller
{
    private function validateRules($display = NULL) {
        return [
            'name' => ['required', Rule::unique('displays')->ignore($display)->where(function ($query) {
                return $query->where('user_id', Auth::id());
            }), 'max:150'],
            'description' => ['max:500'],
            'password' => ['max:150'],
            'show_datetime' => ['boolean']
        ];
    }

    public function index() {
        $displays = Auth::user()->displays()->orderBy('name')->get();

        return view('displays.index', ['displays' => $displays]);
    }

    public function store(Request $request) {
        $validatedData = $request->validate($this->validateRules());

        $display = Auth::user()->displays()->create($validatedData);

        if($request->input('add_and_manage')) {
            return redirect("displays/$display->id");
        } else {
            return redirect('displays');
        }
    }

    public function show(Request $request, Display $display) {
        Gate::authorize('is-display-owner', $display);

        $display->load('screens');

        $request->flush();

        return view('displays.show', ['display' => $display, 'isEditing' => false]);
    }

    public function form(Request $request, Display $display) {
        Gate::authorize('is-display-owner', $display);

        $request->flush();

        return view('displays.form-edit-container', ['display' => $display, 'isEditing' => false]);
    }

    public function update(Request $request, Display $display) {
        Gate::authorize('is-display-owner', $display);

        $validator = Validator::make($request->all(), $this->validateRules($display));
        $validationPasses = $validator->passes();
        if ($validationPasses) {
            $validatedData = $request->only(['name', 'description', 'password']);
            $validatedData['show_datetime'] = $request->has('show_datetime');

            $display->fill($validatedData);
            $display->save();

            $request->flush();
        } else {
            $request->flash();
        }

        return view('displays.form-edit-container', ['display' => $display, 'isEditing' => ($validationPasses == false)])
            ->withErrors($validator);
    }

    public function delete(Request $request, Display $display) {
        Gate::authorize('is-display-owner', $display);

        if ($display->name == $request->input('name')) {
            $display->delete();

            return ['success' => true];
        } else {
            return ['success' => false];
        }
    }
}
