<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function form() {
        return view('auth.login');
    }

    public function login(Request $request) {
        $credentials = $request->only(['email', 'password']);
        $remember = $request->filled('remember_me');

        if (Auth::attempt($credentials, $remember)) {
            return redirect()->intended('displays');
        } else {
            return back()->withInput()->withErrors(['credentials' => 'Invalid credentials, verify your email and password.']);
        }
    }
}
