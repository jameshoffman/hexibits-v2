<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class SignUpController extends Controller
{
    public function form() {
        return view('auth.sign-up');
    }

    public function store(Request $request) {
        $signUpToken = config('auth.sign_up_token');

        $validatedData = $request->validate([
            'email' => ['required', 'unique:users', 'max:150', 'email'],
            'password' => ['required', 'max:191', 'confirmed'],
            'password_confirmation' => ['required'],
            'sign_up_token' => ['required', 'in:'.$signUpToken]
        ]);

        $user = User::create($validatedData);
        Auth::login($user);

        return redirect('/displays');
    }
}
