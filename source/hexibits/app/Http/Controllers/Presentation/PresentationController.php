<?php

namespace App\Http\Controllers\Presentation;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Screens\ResourcesAccessController;
use App\Model\Display;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PresentationController extends Controller
{
    public function show(Request $request, Display $display) {
        ResourcesAccessController::verifyAccess(Auth::user(), $display, $request->get('password', null));

        return view('presentation.show', ['display' => $display]);
    }

    public function load(Request $request, Display $display) {
        ResourcesAccessController::verifyAccess(Auth::user(), $display, $request->get('password', null));

        $lastUpdated = DB::select('select max(updated_at) as updated_at from (
            select updated_at from displays d where d.id = ?
            union
            select max(updated_at) from screens s where s.display_id = ?) t',
        [$display->id, $display->id])[0]->updated_at;

        if ($request->get('lastUpdated', null) < $lastUpdated) {
            $display->load('screens');

            $display->screens->makeHidden(['created_at', 'updated_at', 'display_id']);
            $display->makeHidden(['id', 'password', 'created_at', 'updated_at', 'user_id']);

            return ['updated_at' => $lastUpdated, 'display' => $display];
        } else {
            return ['updated_at' => null];
        }
    }
}
