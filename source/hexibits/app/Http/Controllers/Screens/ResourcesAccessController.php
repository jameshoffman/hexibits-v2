<?php

namespace App\Http\Controllers\Screens;

use App\Http\Controllers\Controller;
use App\Model\Screen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\File;

class ResourcesAccessController extends Controller
{
    public static function verifyAccess($user, $display, $password) {
        $userId = $user ? $user->id : -1;
        $isNotOwner = $userId != $display->user_id;
        $passwordInvalid = $display->password != $password;

        if ($isNotOwner && ($display->password && $passwordInvalid)) {
            abort(403);
        }

        return true;
    }

    public function show(Request $request, $filename) {
        $search = $filename; // it's a single file

        if (dirname($filename) != '.') { // it's a linked resource from a zip file
            $search = explode('/', $filename)[0];
        }

        $screen = Screen::with('display')->where('source', 'like', "%{$search}%")->firstOrFail();

        $this::verifyAccess(Auth::user(), $screen->display, $request->get('password', null));

        $path = storage_path("app/public/screens-resources/{$filename}");

        // $mimeType = File::mimeType($path); // Not working ???
        $mimes = new \Mimey\MimeTypes;
        $extension = pathinfo(storage_path($path), PATHINFO_EXTENSION);
        $mimeType = $mimes->getMimeType($extension);

        return response()->file($path, [
            'Content-Type' => "{$mimeType}"
        ]);
    }
}
