<?php

namespace App\Http\Controllers\Screens;

use App\Http\Controllers\Controller;
use App\Model\Display;
use App\Model\Screen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

class ScreensController extends Controller
{
    private function validateRules() {
        return [
            'title' => ['max:150'],
            'duration' => ['integer', 'min:1', 'max:999'],
            'background_color' => ['regex:/^#[[:xdigit:]]{6}$/'],
            'source_text' => ['max:500'],
            'source_file' => ['sometimes', 'file', 'mimes:jpg,jpeg,png,mp4,html,zip'],
        ];
    }

    private function getValidatedData($request, $rules, $currentType = 'color') {
        $validatedData = $request->validate($rules);

        if ($request->hasFile('source_file')) {
            $file = $request->file('source_file');

            $validatedData['source'] = '/screens-resources/';

            $fileExtension = $file->extension();

            if (in_array($fileExtension, ['html', 'zip'])) {
                $validatedData['type'] = 'html';
            } else {
                $validatedData['type'] = $fileExtension == 'mp4' ? 'video' : 'image';
            }
        } else if (isset($validatedData['source_text'])){
            $validatedData['source'] = $validatedData['source_text'];
            $validatedData['type'] = 'url';
        } else {
            $validatedData['type'] = $currentType;
        }

        return $validatedData;
    }

    public function store(Request $request, Display $display) {
        Gate::authorize('is-display-owner', $display);

        $rules = $this->validateRules();
        $rules['screen_id'] = [
            'integer',
            Rule::exists('screens', 'id')->where(function ($query) use ($display) {
                $query->where('display_id', $display->id);
            })
        ];
        $rules['is_append'] = ['boolean'];

        $validatedData = $this->getValidatedData($request, $rules);

        $newScreen = new Screen($validatedData);

        if ($request->hasFile('source_file')) {
            $this->saveFile($request->file('source_file'), $validatedData, $newScreen);
        }

        if (isset($validatedData['screen_id'])) {
            $screenInsertPosition = Screen::find($validatedData['screen_id'])->position;

            if ($validatedData['is_append']) {
                $screenInsertPosition += 1;
            }

            $newScreen->position = $screenInsertPosition;
        } else {
            $newScreen->position = 0;
        }

        $display->screens()->where('position', '>=', $newScreen->position)->increment('position');
        $display->screens()->save($newScreen);

        return json_encode($display->screens, JSON_UNESCAPED_SLASHES);
    }

    public function update(Request $request, Screen $screen) {
        Gate::authorize('is-screen-owner', $screen);

        $validatedData = $this->getValidatedData($request, $this->validateRules(), $screen->type);

        if (($screen->has_file && in_array($validatedData['type'], ['url', 'color']))
            || isset($validatedData['source_file'])) {

                $screen->deleteFile();
        }

        if (isset($validatedData['source_file'])) {
            $this->saveFile($request->file('source_file'), $validatedData, $screen);

            unset($validatedData['source']); // remove so it does not override source
                                             // assigne in saveFile()
        }

        $screen->fill($validatedData);
        $screen->save();

        return $screen->attributesToArray();
    }

    private function saveFile($file, $data, $screen) {
        if ($file->extension() == 'zip') {
            $subfolder = (string) Str::uuid();
            $path = "app/public/screens-resources/{$subfolder}";

            $zip = new \ZipArchive;
            $zip->open($file->getRealPath());
            $zip->extractTo(storage_path($path) . '/');
            $zip->close();

            $screen->source = "/screens-resources/{$subfolder}/index.html";
        } else {
            $path = $file->store('', 'screens-resources');
            $screen->source = $data['source'] . $path;
        }
    }

    public function delete(Screen $screen) {
        Gate::authorize('is-screen-owner', $screen);

        $screen->display->screens()->where('position', '>', $screen->position)->decrement('position');
        $screen->display->touch();

        $screen->delete();

        return true;
    }

    public function reorder(Request $request, Screen $screen) {
        Gate::authorize('is-screen-owner', $screen);

        $maxPosition = $screen->display()->withCount('screens')->first()->screens_count;
        $position = $request->get('position', null);

        if ($position != null && $position < $maxPosition) {
            if ($position > $screen->position) {
                // destination is greater
                $screen->display->screens()->where([
                    ['position', '>', $screen->position],
                    ['position', '<=', $position]
                ])->decrement('position');
            } else {
                // destination is lower
                $screen->display->screens()->where([
                    ['position', '>=', $position],
                    ['position', '<', $screen->position]
                ])->increment('position');
            }

            $screen->position = $position;
            $screen->save();
        }

        return false;
    }
}
