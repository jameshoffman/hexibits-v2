<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Screen extends Model
{
    protected static function booted()
    {
        static::addGlobalScope('position', function (Builder $builder) {
            $builder->orderBy('position');
        });
    }

    protected $fillable = [
        'source', 'title', 'duration', 'background_color', 'type', 'position'
    ];

    protected $casts = [
        'duration' => 'int'
    ];

    public function display() {
        return $this->belongsTo('App\Model\Display');
    }

    public function getIsImageAttribute() {
        return $this->type == 'image';
    }

    public function getIsVideoAttribute() {
        return $this->type == 'video';
    }

    public function getIsHtmlAttribute() {
        return $this->type == 'html';
    }

    public function getIsUrlAttribute() {
        return $this->type == 'url';
    }

    public function getIsEmptyAttribute() {
        return $this->type == 'color';
    }

    public function getHasFileAttribute() {
        return $this->is_image || $this->is_video || $this->is_html;
    }

    public function delete() {
       $this->deleteFile();

       return parent::delete();
    }

    public function deleteFile() {
        if ($this->has_file) {
            $sourceFolder = dirname($this->source);

            if (basename($sourceFolder) != 'screens-resources') {
                // was a zip, has a subfolder
                Storage::disk('public')->deleteDirectory($sourceFolder);
            } else {
                Storage::disk('public')->delete($this->source);
            }
        }
    }
}
