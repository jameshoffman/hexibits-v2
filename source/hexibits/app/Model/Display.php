<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Display extends Model
{
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'show_datetime', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'show_datetime' => 'boolean'
    ];

    public function user() {
        return $this->belongsTo('App\Model\User');
    }

    public function screens() {
        return $this->hasMany('App\Model\Screen');
    }

    public function delete() {
        $this->screens()->get()->each->delete();
        // Needed to apply custom delete logic
        // Mass delete using only the relationship uses th DB directly
        // bypassing custom delete() in Screen

        return parent::delete();
     }
}
