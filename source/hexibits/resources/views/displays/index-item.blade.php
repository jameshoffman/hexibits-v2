<!--
Parameters:

['display' => $display]

-->

<tr style="cursor: pointer;" onclick="window.location = '{{ url('displays', $display->id) }}';">
    <td style="width: 59px">
        @if ($display->password)
            <i class="fas fa-lock text-danger align-top" style="padding-top: 6px;"></i>
        @endif

        @if ($display->show_datetime)
            <i class="far fa-clock text-info align-top" style="padding-top: 6px"></i>
        @endif
    </td>

    <td>
        <div class="row">
            <div class="col">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <span class="h4">{{ $display->name }}</span>
                    </div>
                    <div class="col text-truncate" style="line-height: 29px; padding-top: 2px;">
                        {{ $display->description }}
                    </div>
                </div>
            </div>
        </div>
    </td>

    <td style="width: 48px;">
        <i class="fas fa-lg fa-chevron-right text-black-50 align-top mx-2" style="padding-top: 6px"></i>
    </td>
</tr>
