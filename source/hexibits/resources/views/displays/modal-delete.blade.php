<!--
Parameters:

['displayId' => INT]
-->

<div id="deleteModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Are you sure?</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <p>Enter the display's name to delete it</p>

                <input id="inputNameDelete" type="text" class="form-control" placeholder="Display name" name="name">
                <div class="invalid-feedback">
                    The display name does not match.
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary text-muted" data-dismiss="modal">Cancel</button>
                <button id="buttonDelete" type="submit" class="btn btn-danger">Delete</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('#buttonDelete').click(function() {
        $.ajax({
            url: url('displays/{{ $displayId }}'),
            method: "DELETE",
            headers: { 'X-CSRF-TOKEN': csrf() },
            data: { name: $('#inputNameDelete').val() }
        })
        .done(function( response ) {
            if (response.success == true) {
                window.location = '{{ url('displays') }}';
            } else {
                $('#inputNameDelete').addClass('is-invalid');
            }
        });
    });

    $('#deleteModal').on('hidden.bs.modal', function () {
        $('#inputNameDelete').val(null);
        $('#inputNameDelete').removeClass('is-invalid');
    });
</script>
