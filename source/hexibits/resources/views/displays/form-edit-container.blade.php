<!--
Parameters:

['isEditing' => 'boolean', 'display' => $displays | NULL]
-->

<div id="form-edit-container">
    @include('partials.errors')

    <form id="formEditDisplay" action="{{ url('displays', $display->id) }}">
        @include('displays.form', ['isEditing' => $isEditing, 'display' => $display])

        <div class="row">
            <div class="col-auto">
                @php
                    $showUrl = url('/show/' . $display->name);

                    if ($display->password) {
                        $showUrl .= "?password={$display->password}";
                    }
                @endphp
                <a class="btn btn-outline-primary" href="{{ $showUrl }}" target="_blank" role="button">Preview <i class="fas fa-external-link-alt"></i></a>
            </div>

            <div id="formButtonsContainer" class="col text-right">
                <button type="button" id="buttonEdit" class="btn btn-warning {{ $isEditing ? 'd-none' : ''}}">Edit</button>

                <button type="button" id="buttonCancel" class="btn btn-secondary text-muted mr-4 {{ $isEditing ? '' : 'd-none'}}">Cancel</button>
                <button type="submit" id="buttonSave" class="btn btn-success {{ $isEditing ? '' : 'd-none'}}">Save</button>
            </div>
        </div>
    </form>
    <script>
        $('#buttonEdit').click(function(evt) {
            $('#formButtonsContainer button').toggleClass('d-none');
            $('#formEditDisplay input').prop('disabled', false);
        });

        $('#buttonCancel').click(function() {
            $.ajax({
                url: url('displays/{{ $display->id }}/form'),
                method: "GET",
                headers: { 'X-CSRF-TOKEN': csrf() }
            }).done(function( response ) {
                $('#form-edit-container').html(response);
            });
        });

        $('#buttonSave').click(function(evt) {
            evt.preventDefault();

            $.ajax({
                url: url('displays/{{ $display->id }}'),
                method: "PATCH",
                headers: { 'X-CSRF-TOKEN': csrf() },
                data: $('#formEditDisplay').serialize()
            }).done(function( response ) {
                $('#form-edit-container').html(response);
            });
        });
    </script>
</div>
