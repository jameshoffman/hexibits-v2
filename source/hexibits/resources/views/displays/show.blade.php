@extends('master')

@section('title', $display->name . ' Details')

@section('container')
<div class="row my-4 align-items-baseline">
    <div class="col h1">
        <a href="{{ url('displays') }}" role="button" class="text-black-50 align-top mr-1" style="font-size: smaller;"><i class="fas fa-chevron-left" style=" padding-top: 7px;"></i></a>
        Manage display
    </div>
    <div class="col-auto"><button class="btn btn-sm btn-outline-danger mb-2 ml-2" data-toggle="modal" data-target="#deleteModal">Delete</button></div>

    @include('displays.modal-delete', ['displayId' => $display->id])
</div>

@include('displays.form-edit-container', ['display' => $display])

<div class="mt-5">
    @include('screens.index', ['screens' => $display->screens])
</div>

@endsection
