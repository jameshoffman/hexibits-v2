@extends('master')

@section('title', 'Displays')

@section('container')
    <h1 class="my-4">Displays</h1>

    @include('partials.errors')

    <form action="{{ url('displays') }}" method="POST">
        @csrf
        @include('displays.form', ['isEditing' => true])

        <div class="text-right">
            <button type="submit" class="btn btn-success float-right">Add</button>
            <button class="btn btn-outline-success mr-4" name="add_and_manage" value="true">Add and manage screens</button>
        </div>
    </form>

    <table class="table table-hover mt-5" style="table-layout: fixed;">
        @forelse ($displays as $display)
            @include('displays.index-item', ['display' => $display])
        @empty
            <caption class="font-italic text-center">No display to show...</caption>
        @endforelse
    </table>
@endsection
