<!--
Parameters:

['isEditing' => 'boolean', 'display' => $displays | NULL]
-->
@php
    $disabled = $isEditing ? '' : 'disabled';

@endphp

<div class="form-row">
    <div class="form-group col-12">
      <input type="text" class="form-control" id="inputName" placeholder="Name" name="name" {{ $disabled }} value="{{ old('name') ?? $display->name ?? '' }}">
    </div>
</div>

<div class="form-row">
    <div class="form-group col-12">
        <fieldset class="border p-2">
            <legend class="w-auto px-1 h6 text-muted font-italic">Optional settings</legend>

            <div class="form-row">
                <div class="form-group col-12">
                    <input type="text" class="form-control" id="inputDescription" placeholder="Description" name="description" {{ $disabled }} value="{{ old('description') ?? $display->description ?? '' }}">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-sm-auto pt-sm-2">
                    <div class="custom-control custom-switch">
                        <input type="checkbox" value="1" class="custom-control-input" id="inputShowDatetime" name="show_datetime" {{ $disabled }} {{ (old('show_datetime') ?? $display->show_datetime ?? false) ? 'checked' : ''}}>
                        <label class="custom-control-label" for="inputShowDatetime">Show date and time?</label>
                    </div>
                </div>

                <div class="form-group col-sm pl-sm-4">
                  @include('partials.helpers.input-password',
                          ['id' => 'inputPassword', 'name' => 'password', 'placeholder' => 'Password', 'required' => '', 'disabled' => $disabled, 'value' =>  (old('password') ?? $display->password ?? '')])
                </div>
            </div>
        </fieldset>
    </div>
</div>
