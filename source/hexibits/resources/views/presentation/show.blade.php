<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="base-url" content="{{ url('') }}">

        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">

        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('js/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('js/helpers.js') }}"></script>

        <title>{{ $display->name }}</title>

        <style>
            :root {
                --banner-height: 100px;
                --title-span-width: 0px;
                --title-margin: 128px;
                --title-animation-duration: 5s;
                --datetime-width: 150px;
            }

            body {
                cursor: none;
            }

            .preview {
                width: 100%;
                height: 100%;
                object-fit: contain;
            }

            #banner {
                background-color: white;
                overflow: hidden;
                height: var(--banner-height);
            }

            #source.with-banner {
                height: calc(100% - var(--banner-height));
            }

            #banner.with-banner {
                margin-top: 0px;
            }

            #source.no-banner {
                height: 100%;
            }

            #banner.no-banner {
                margin-top: var(--banner-height);
            }

            .title {
                white-space: nowrap;
            }

            #titleAnimationContainer {
                animation: title-marquee var(--title-animation-duration) linear infinite;
                animation-play-state: paused;
            }

            #datetime {
                width: var(--datetime-width);
            }

            @keyframes title-marquee {
                0% { margin-left: 0; }
                100% { margin-left: calc(-1 * var(--title-span-width) - var(--title-margin)) };
            }
        </style>
    </head>
    <body>
        <div style="height: 100vh; overflow: hidden;" >
            <div id="source" class="section no-banner d-flex justify-content-around" style="background-color: black;">
                <div id="loading" class="h1 text-center align-self-center">
                    Loading <i>{{ $display->name }}</i>
                </div>

                <div id="color" class="preview currentPreview" style="display: none;"></div>

                <img id="image" class="preview" style="display: none;">

                <video id="video" class="preview" style="display: none;" muted loop>
                    <source type="video/mp4">
                </video>

                <iframe id="url" class="preview" style="display: none;" frameborder="0" scrolling="no"></iframe>
                <iframe id="html" class="preview" style="display: none;" frameborder="0" scrolling="no"></iframe>
            </div>

            <div id="banner" class="section no-banner border-top d-flex">
                <div id="titleContainer" class="display-4 pl-4" style="width: calc(100% - var(--datetime-width) - 24px); line-height: var(--banner-height); overflow: hidden;">
                    <span id="titleAnimationContainer" style="display: none;">
                        <span class="title"></span><span class="title title-animate" style="display: none; margin-left: var(--title-margin);"></span>
                    </span>
                </div>

                <div id="datetime" class="d-flex flex-column justify-content-center text-center mx-4">
                    <div id="date" style="letter-spacing: 0.5px;"></div>
                    <div id="time" class="h1"></div>
                </div>
            </div>
        </div>


        <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
        <script>
            const password = new URLSearchParams(window.location.search).get('password');
            let lastUpdated = null;
            let display = null;
            let currentScreenIndex = -1;

            let nextSlideTimeout = null;
            let titleAnimationTimeout = null;
            screenUpdateTimeout = null;

            $(document).ready(function() {
                updateDatetime();
                setInterval(updateDatetime, 30000);

                refresh();
            });

            function showLoadingError() {
                clearTimeouts()

                $('.preview').hide(500);
                const sections = $('.section');
                if (sections.hasClass('with-banner')) {
                    sections.toggleClass('no-banner with-banner', 500);
                }

                const loading = $('#loading');
                loading.text('Could not load screen...');
                loading.addClass('text-danger');

                loading.delay(1000).fadeIn(500);
            }

            function updateDatetime() {
                const now = new Date();
                const date = `${String(now.getDate()).padStart(2, '0')}/${String(now.getMonth() + 1).padStart(2, '0')}/${now.getFullYear()}`;
                const time = `${now.getHours()}:${String(now.getMinutes()).padStart(2, '0')}`;

                $('#date').text(date);
                $('#time').text(time);
            }

            function clearTimeouts() {
                clearTimeout(nextSlideTimeout);
                clearTimeout(titleAnimationTimeout);
                clearTimeout(screenUpdateTimeout);
            }

            function refresh() {
                const request = $.ajax({
                    url: url(`show/{{ $display->name }}/load`),
                    method: "GET",
                    data: {
                        password: password,
                        lastUpdated: lastUpdated
                    },
                });

                request.done(function(response) {
                    if (request.status == 200 && response.updated_at) {
                        display = response.display;

                        // head title
                        $('title').text(`${display.name}`);

                        if (lastUpdated == null) {
                            // first data load

                            $('#loading').delay(1000).fadeOut(500, function() {
                                if (display.screens.length > 0) {
                                    nextSlide();
                                }

                                setInterval(refresh, 15000);
                            });
                        } else {
                            // data changed, reload
                            clearTimeouts();

                            $('#loading').hide();
                            $('.preview').hide(500);
                            const sections = $('.section');
                            if (sections.hasClass('with-banner')) {
                                sections.toggleClass('no-banner with-banner', 500);
                            }

                            $('.preview, .section').promise().done(function() {
                                currentScreenIndex = -1; //reset current screen

                                screenUpdateTimeout = setTimeout(function() {
                                    if (display.screens.length > 0) {
                                        nextSlide();
                                    }
                                }, 1000) // Delay so screen does not flicker, stay on black screen a bit
                            })
                        }

                        lastUpdated = response.updated_at;
                    } else if (request.status != 200) {
                        showLoadingError();
                    }
                });

                request.fail(showLoadingError);
            }

            function nextSlide() {
                const lastScreenId = currentScreenIndex >= 0 ? display.screens[currentScreenIndex].id : -1;
                currentScreenIndex = (++currentScreenIndex % display.screens.length); // cycle through screens index

                const nextScreen = display.screens[currentScreenIndex];

                if (nextScreen.id != lastScreenId) {
                    const currentPreview = $('.currentPreview');
                    const titleAnimationContainer = $('#titleAnimationContainer');
                    const titleView = $('.title');
                    const sections = $('.section');

                    if (display.show_datetime == false && nextScreen.title == null && sections.hasClass('with-banner')) {
                        sections.toggleClass('no-banner with-banner', 500);
                    }

                    if ($('#banner').hasClass('with-banner')) { // Only animate if banner is currently visible
                        titleAnimationContainer.fadeOut(500);
                    }

                    currentPreview.fadeOut(500, function() {
                        currentPreview.removeClass('currentPreview');

                        const nextPreview = $(`#${nextScreen.type}`);

                        nextPreview.addClass('currentPreview');
                        nextPreview.css('background-color', nextScreen.background_color);

                        // Wait for source to be ready
                        if (nextScreen.type == 'video') {
                            nextPreview.on('loadeddata', function() {
                                nextPreview.off('loadeddata');

                                showSlide(nextPreview, nextScreen);
                            });
                        } else if (nextScreen.type == 'color') {
                            showSlide(nextPreview, nextScreen);
                        } else {
                            nextPreview.on('load', function() {
                                nextPreview.off('load');

                                showSlide(nextPreview, nextScreen);
                            });
                        }

                        // Setting src will eventually trigger load/loadeddata

                        const srcPassword = nextScreen.type != 'url' ? `?password=${password}` : '';

                        nextPreview.attr('src', `${nextScreen.source}${srcPassword}`);

                        // Handling title
                        titleView.text(nextScreen.title);

                        $('.title-animate').hide();
                        // reset marquee animation
                        titleAnimationContainer.css('animation-play-state', 'paused');
                        titleAnimationContainer.css('animation', 'none');

                        // datetime
                        const datetimeView = $('#datetime');
                        if (display.show_datetime) {
                            datetimeView.removeClass('d-none');
                            datetimeView.addClass('d-flex');
                            document.documentElement.style.setProperty('--datetime-width', '150px');
                        } else {
                            datetimeView.removeClass('d-flex');
                            datetimeView.addClass('d-none');
                            document.documentElement.style.setProperty('--datetime-width', '0px');
                        }

                        // Handling banner
                        if ((display.show_datetime || nextScreen.title)  && sections.hasClass('no-banner')) {
                            sections.toggleClass('no-banner with-banner', 500);
                        }
                    });
                }
            }

            function showSlide(preview, screen) {
                const titleAnimationContainer = $('#titleAnimationContainer');
                if (screen.title) {
                    titleAnimationContainer.delay(200).fadeIn(500);
                }

                preview.delay(300).fadeIn(500);

                // titleAnimationContainer must be displayed so width is calculated
                // wait for both animatoins to finish
                $('preview, #titleAnimationContainer').promise().done(function() {
                    let duration = screen.duration;
                    if (screen.type == 'video') {
                        const video = preview[0];
                        video.play();
                        duration = video.duration;
                    }

                    const titleSpanWidth = $('.title').first().width();
                    const titleContainerWidth = $('#titleContainer').first().width();
                    if (titleSpanWidth > titleContainerWidth) {
                        $('.title-animate').show();

                        document.documentElement.style.setProperty('--title-span-width', `${titleSpanWidth}px`);

                        const duration = 0.015 * titleSpanWidth; // factor * distance = duration, increase factor to go slower(duration will be bigger)
                        document.documentElement.style.setProperty('--title-animation-duration', `${duration}s`);

                        titleAnimationContainer.addClass('animate-marquee');
                        titleAnimationContainer.css('animation', '');
                    } else {
                        titleAnimationContainer.removeClass('animate-marquee');

                        $('.title-animate').hide();
                    }

                    const animationState = titleAnimationContainer.hasClass('animate-marquee') ? 'running' : 'paused';
                    titleAnimationTimeout = setTimeout(function() { titleAnimationContainer.css('animation-play-state', animationState); }, 500);

                    nextSlideTimeout = setTimeout(nextSlide, duration * 1000); // * 1000 because duration is in seconds
                });
            }

        </script>
    </body>
</html>
