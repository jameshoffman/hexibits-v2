<nav class="navbar navbar-expand-sm navbar-dark bg-primary">
    <div class="container">
      <a class="navbar-brand font-weight-bold" href="{{ url('') }}">hexibits</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample07" aria-controls="navbarsExample07" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExample07">
        <!-- Remove it because there was a single nav item, clicking the hexibits title navigates to /
        <ul class="navbar-nav mr-auto">
            <li class="nav-item {{ Request::is('displays') ? 'active' : ''}}">
                <a class="nav-link" href="{{ url('displays') }}">Displays <span class="sr-only">(current)</span></a>
            </li>
        </ul>
        -->

        <ul class="navbar-nav ml-auto">
            <li class="nav-item navbar-text">
                <span class="mr-2">{{ Auth::user()->email }}</span>|
            </li>
            <li class="nav-item">
                <form action="{{ url('logout') }}" method="POST">
                    @csrf

                    <button type="submit" class="btn btn-link nav-link">Log out <i class="fas fa-sign-out-alt"></i></button>
                </form>
            </li>
          </ul>
      </div>
    </div>
  </nav>
