<!--
Parameters:

['id' => '', 'name' => '', 'placeholder'=> '', 'required' => 'required | EMPTY', 'disabled' => 'disabled | EMPTY', 'value'=> '']

-->
<div class="input-group">
<input type="password" class="form-control" id="{{ $id }}" name="{{ $name }}" placeholder="{{ $placeholder }}" {{ $required }} {{ $disabled }} value="{{ $value }}">
    <div class="input-group-append">
        <button class="btn btn-primary" type="button" id="{{$id}}showToken"><i class="fa fa-sm fa-eye-slash" id="{{$id}}showIcon" aria-hidden="true"></i></button>
        <script>
            $('#{{$id}}showToken').click(function() {
                $('#{{$id}}').attr('type',  function(index, attr) {
                    return attr == 'password' ? 'text' : 'password';
                });

                $('#{{$id}}showIcon').toggleClass('fa-eye fa-eye-slash');
            })
        </script>
    </div>
</div>
