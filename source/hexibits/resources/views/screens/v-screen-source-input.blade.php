<!--
    SCREEN PREVIEW
-->
@verbatim
<script>
    screensListApp.component('screen-source-input', {
        template: '#screen-source-input-template',

        props: ['screen', 'isEditing'],

        data() {
            return this.init();
        },

        computed: {
            source: {
                get() {
                    if (this.isType('url')) {
                        return this.screen.source;
                    } else if (this.screen.source != this.originalSource) {
                        return this.pickedFile?.name;
                    }
                },

                set(value) {
                    if (value instanceof File) {
                        const path = value.name.toLowerCase();

                        if (path.endsWith('.mp4')) {
                            this.screen.type = 'video';
                        } else if (path.endsWith('.jpg') || path.endsWith('.jpeg') || path.endsWith('.png')) {
                            this.screen.type = 'image';
                        } else if(path.endsWith('.html') || path.endsWith('.zip')) {
                            this.screen.type = 'html'
                        }

                        if (path.endsWith('.zip')) {
                            const zipTempMessage = 'data:text/html;charset=utf-8,<h1>Zip content will be available after saving.</h1>'
                            this.screen.source = encodeURI(zipTempMessage);
                        } else {
                            this.screen.source = URL.createObjectURL(value);
                        }


                        this.pickedFile = value;
                    } else {
                        if (value?.toLowerCase().startsWith('http')) {
                            this.screen.type = 'url';
                        } else {
                            this.screen.type = 'color';
                        }

                        this.screen.source = value;
                        this.pickedFile = null;
                    }
                }
            }
        },

        methods: {
            init() {
                return {
                    originalType: this.screen.type,
                    originalSource: this.screen.source,
                    pickedFile: null
                }
            },

            isType(type) {
                return this.screen.type == type;
            },

            toggleType() {
                if (this.isType('url')) {
                    this.screen.type = (this.originalType != 'url' ? this.originalType : 'color');
                } else {
                    this.screen.type = 'url';
                }

                this.screen.source = (this.isType(this.originalType) ? this.originalSource : null);
            },

            filePicked(event) {
                const file = event.target.files[0];

                if (file) {
                    this.source = file;
                    this.$refs.picker.value = null;
                    // file input mus be clearer so input event is trigger even when selecting the same file again
                    // EX: select a file, change to url, change back to file, select the same file
                }
            },

            confirm() {
                // This method should be called when parent components save screen data

                const init = this.init();

                for (const key in init) {
                    this.$data[key] = init[key];
                }
            }
        }
    });
</script>

<template id="screen-source-input-template">

    <div v-show="isEditing" class="input-group">
        <div class="input-group-prepend">
            <button @click="toggleType()" class="btn btn-sm btn-primary" type="button">
                <i :class="['fas', isType('url') ? 'fa-link' : 'fa-photo-video']" style="width: 16px;"></i>
            </button>
        </div>

        <input v-model.lazy="source" :placeholder="isType('url') ? 'URL' : 'Choose file (jpg/png, mp4, html/zip)'" :disabled="! isType('url')" type="text" class="form-control" required>
        <div class="input-group-append">
            <input ref="picker" @change="filePicked" class="d-none" type="file" accept=".jpg,.jpeg,.png,.mp4,.html,.zip">
            <button @click="this.$refs.picker.click()" v-show="! isType('url')" class="btn btn-sm btn-primary" type="button"><i class="fas fa-file-upload" style="width: 16px;"></i></button>
        </div>
    </div>
</template>
@endverbatim
