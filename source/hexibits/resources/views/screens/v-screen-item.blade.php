
<!--
    SCREEN ITEM
-->
@verbatim
<script>
    screensListApp.component('screen-item', {
        props: ['screen'],
        template: '#screen-item-template',

        data() {
            return {
                isEditing: false,
                originalScreen: Object.freeze({ ...this.screen })
            }
        },

        computed: {
            previewStyle() {
                const height = (this.isEditing ? 94 : 51);

                return {
                    width: '100%',
                    height: `calc(100% - ${height}px)`
                }
            }
        },

        methods: {
            isType(type) {
                return this.screen.type == type;
            },

            add(isAppend) {
                this.$parent.addScreen(isAppend, this.screen);
            },

            edit() {
                this.isEditing = true;

                this.originalScreen = Object.freeze({ ...this.screen });
            },

            cancel() {
                this.isEditing = false;

                for (const key in this.screen) {
                    this.screen[key] = this.originalScreen[key];
                }
            },

            save() {
                const sourceInput = this.$refs.sourceInput;
                const screenItem = this;
                const screenData = { ...this.screen };

                if (screenData.source == this.originalScreen.source) {
                    screenData.source = null;
                }

                this.$parent.submitScreen(url(`screens/${screenData.id}`), 'PATCH', screenData, sourceInput.pickedFile, function(response) {
                    screenItem.isEditing = false;

                    screenItem.screen.source = response.source;

                    sourceInput.confirm();
                });
            },

            remove() {
                this.$parent.deleteScreen(this.screen);
            }
        }
    });
</script>

<template id="screen-item-template">

    <div :data-screen-id="screen.id" class="v-screen-item col-12 col-sm-6 col-lg-4 square p-2 screen-container">

        <div class="card h-100">
            <div class="card-header text-center bg-light" style="cursor: pointer;">
                <div class="row">
                    <div class="col text-truncate pl-1 pr-3" style="height: 31px;">
                        <span v-show="! isEditing">
                            <span v-if="screen.title" class="h5" style="line-height: 33px;">{{ screen.title }}</span>
                            <span v-else class="h5 text-muted font-italic" style="line-height: 33px;">Untitled</span>
                        </span>

                        <input v-show="isEditing" v-model="screen.title" class="form-control form-control-sm py-0" placeholder="Title" name="title">
                    </div>

                    <div v-show="isEditing" class="col-auto p-0">
                        <button @click="remove()" type="button" class="btn btn-link text-danger mr-3 p-0 border-0" style="font-size: 18px; height: 31px;">
                            <i class="far fa-trash-alt"></i>
                        </button>
                    </div>
                </div>
            </div>


            <div class="card-body pb-0 pt-2 pl-2 pr-2">
                <screen-preview :style="previewStyle" :screen="screen" :is-editing="isEditing"></screen-preview>

                <screen-source-input ref="sourceInput" class="mt-1 mb-1" :screen="screen" :is-editing="isEditing"></screen-source-input>

                <div class="row pt-2 no-gutters">
                    <div class="col text-truncate" style="line-height: 31px;">
                        <div class="row  no-gutters">
                            <div class="col-auto">
                                <i class="fas fa-lg fa-stopwatch mr-2 text-muted"></i>
                            </div>

                            <div v-show="! isEditing" v-text="screen.duration + ' seconds'" class="col-auto"></div>

                            <div v-show="isEditing" class="col-auto pr-3">
                                <input v-model="screen.duration" type="number" min="1" max="99" class="screen-duration-edit form-control form-control-sm py-0 w-auto">
                            </div>

                            <div v-show="isEditing" class="col-auto">
                                <i class="fas fa-lg fa-eye-dropper mr-1 text-muted align-top d-md-inline-block" style="padding-top: 9px;"></i>
                                <input v-model="screen.background_color" type="color" class="screen-background-color-edit border-0 py-0" style="width: 28px; height: 28px;">
                            </div>

                        </div>
                    </div>
                    <div class="col-auto pr-2">
                        <button v-show="! isEditing" @click="edit()" type="button" class="btn btn-sm btn-warning"><i class="fas fa-pencil-alt"></i></button>
                    </div>

                    <div class="col-auto">
                        <div class="dropdown">
                            <button v-show="! isEditing" type="button" class="btn btn-sm btn-success" data-toggle="dropdown"><i class="fas fa-plus"></i></button>

                            <div class="dropdown-menu dropdown-menu-right" style="min-width: 1px;">
                                <button @click="add(false)" class="dropdown-item" type="button"><i class="fas fa-angle-left align-top" style="padding-top: 3px;"></i> Insert</button>
                                <button @click="add(true)" class="dropdown-item" type="button">Append <i class="fas fa-angle-right align-top" style="padding-top: 4px;"></i></button>
                            </div>
                        </div>
                    </div>

                    <div v-show="isEditing" class="col-auto">
                        <button @click="cancel()" class="btn btn-sm btn-secondary text-muted mr-2"><i class="fas fa-times"></i></button>
                        <button @click="save()" class="btn btn-sm btn-success"><i class="fas fa-check"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</template>
@endverbatim
