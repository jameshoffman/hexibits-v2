<!--
    CREATE MODAL
-->
@verbatim
<script>
    const DEFAULT_SCREEN = Object.freeze({
        title: null,
        background_color: '#CCCCCC',
        duration: '10',
        type: 'color',
        source: null
    });


    screensListApp.component('screen-create-modal', {
        template: '#screen-create-modal-template',

        data() {
            return {
                infos: { },
                screen: { ...DEFAULT_SCREEN }
            }
        },

        computed: {
            addActionText: function() {
                if (this.infos.screen) {
                    return this.infos.isAppend ? 'Append after' : 'Insert before';
                } else {
                    return '';
                }
            }
        },

        methods: {
            open(infos) {
                this.$data.infos = infos;
                this.$data.screen = { ...DEFAULT_SCREEN };

                $('#modal-create').modal('show');
            },

            save() {
                const sourceInput = this.$refs.sourceInput;
                const screenData = { ...this.screen };
                const modal = this;

                screenData.display_id = this.infos.displayId;

                if (this.infos.screen) {
                    screenData.screen_id = this.infos.screen.id;
                    screenData.is_append = Number(this.infos.isAppend);
                }

                this.$parent.submitScreen(url(`displays/${this.infos.displayId}/screens`), 'POST', screenData, sourceInput.pickedFile, function(response) {
                    sourceInput.confirm();

                    modal.$parent.refreshList(JSON.parse(response));
                });
            }
        }
    });
</script>

<template id="screen-create-modal-template">

<div id="modal-create" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">
                    <h5>Add screen</h5>

                    <p v-if="infos.screen" v-html="`${addActionText} <i>${infos.screen?.title}</i>`" class="mb-0 font-weight-bold"></p>
                </div>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <screen-preview style="width: 100%; height: 256px;" :screen="screen" :is-editing="true"></screen-preview>

                <screen-source-input ref="sourceInput" class="mt-1 mb-3" :screen="screen" :is-editing="true"></screen-source-input>

                <div class="form-row">
                    <div class="form-group col-6 text-right pr-4">
                        <i class="fas fa-lg fa-stopwatch mr-2 text-muted"></i>
                        <input v-model="screen.duration" type="number" min="1" max="99" class="form-control d-inline-block py-0 w-auto">
                        seconds
                    </div>

                    <div class="form-group col-6 text-left pl-5">
                        <i class="fas fa-lg fa-eye-dropper mr-2 text-muted align-top" style="padding-top: 10px;"></i>
                        <input v-model="screen.background_color" type="color" class="border-0 py-0" style="width: 34px; height: 34px;">
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <input v-model="screen.title" type="text" class="form-control" placeholder="Title">
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary text-muted" data-dismiss="modal">Cancel</button>
                <button @click="save()" type="submit" class="btn btn-success" data-dismiss="modal">Add</button>
            </div>
        </div>
    </div>
</div>

</template>
@endverbatim
