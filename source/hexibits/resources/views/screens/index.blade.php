<!--
Parameters:

['screens' => $screens]
-->
<style>

    .screen-container {
        height: 304px;
    }

    @media (min-width: 576px) {
        .screen-container {
            height: 254px;
        }
    }

    @media (min-width: 768px) {
        .screen-container {
            height: 344px;
        }
    }

    @media (min-width: 992px) {
        .screen-container {
            height: 304px;
        }
    }

    @media (min-width: 1200px) {
        .screen-container {
            height: 364px;
        }
    }

    .screen-preview {
        width: 100%;
        object-fit: contain;
    }
</style>

@if(app()->environment('production'))
    <script src="{{ asset('js/vuejs3.1.1.global.prod.js') }}"></script>
@else
    <script src="{{ asset('js/vuejs3.1.1.global.js') }}"></script>
@endif

<!--
    SCREENS LIST
-->
<div id="screens-list">
    <screen-create-modal ref="createModal"></screen-create-modal>

    <screen-delete-modal ref="deleteModal"></screen-delete-modal>

    <div class="row">
        <div class="col">
            <h3 class="text-muted d-inline-block">Screens</h3><button @click="addScreen(true)" class="btn btn-sm btn-success mb-2 ml-2"><i class="fas fa-plus"></i></button>
        </div>
    </div>

    <div class="row" id="screensGrid">
        <template v-if="screens.length > 0" >
            <screen-item v-for="screen in screens" :key="screen.id" :screen="screen"></screen-item>
        </template>

        <div v-else class="col">
            <p class="text-muted font-italic text-center">No screen to show...</p>
        </div>
    </div>
</div>

<script>
const ScreensList = {
    name: 'ScreensList',

    data() {
        return {
            screens: @json($screens, JSON_UNESCAPED_SLASHES)
        }
    },

    methods: {
        addScreen(isAppend, siblingScreen) {
            const screen = siblingScreen ?? this.screens.slice(-1)[0];
            const infos = {
                displayId: {{ $display->id }},
                screen: screen,
                isAppend: isAppend
            };

            this.$refs.createModal.open(infos);
        },
        submitScreen(url, method, screenData, file, doneCallback) {
            // Used for add and update

            const formData = new FormData();
            formData.append('_method', method);

            for(const key in screenData) {
                formData.append(key, screenData[key] ?? '');
            }

            if (file) {
                formData.append('source_file', file);
            } else {
                formData.append('source_text', screenData.source ?? '');
            }

            $.ajax({
                url: url,
                method: "POST",
                headers: { 'X-CSRF-TOKEN': csrf() },
                data: formData,
                contentType: false,
                processData: false
            })
            .done(doneCallback);
        },

        deleteScreen(screen) {
            this.$refs.deleteModal.open(screen);
        },
        confirmDelete(screen) {
            let request = $.ajax({
                url: url(`screens/${screen.id}`),
                method: "DELETE",
                headers: { 'X-CSRF-TOKEN': csrf() }
            });

            const screenList = this;
            request.done(function( response ) {
                if (request.status == 200) {
                    screenList.screens = screenList.screens.filter(s => s.id != screen.id);
                }
            });
        },

        refreshList(screens) {
            this.screens = screens;
        }
    }
}

const screensListApp = Vue.createApp(ScreensList);

</script>

<!--
    Includes must be placed right there
 -->

@include('screens.v-screen-item')

@include('screens.v-screen-preview')

@include('screens.v-screen-source-input')

@include('screens.v-screen-create-modal')

@include('screens.v-screen-delete-modal')


<!--
    SCREENS LIST MOUNT
-->
<script>
    const screensList = screensListApp.mount('#screens-list');

    $('#screensGrid').ready(function() {
        $('#screensGrid').sortable({
            containment: 'document',
            handle: '.card-header',
            tolerance: "pointer",
            scroll: false,
            forceHelperSize: true,
            placeholder: 'col-12 col-sm-6 col-lg-4 bg-light border',
            cursor: "move",
            stop: function( event, ui ) {
                $.ajax({
                    url: url(`screens/${ui.item.data('screen-id')}/reorder`),
                    method: "POST",
                    headers: { 'X-CSRF-TOKEN': csrf() },
                    data: {
                        position: ui.item.index()
                    }
                });
            }
        });

        $('.card-header').disableSelection();
    });
</script>
