<!--
    DELETE MODAL
-->
@verbatim
<script>
    screensListApp.component('screen-delete-modal', {
        template: '#screen-delete-modal-template',

        data() {
            return {
                screen: null
            }
        },

        methods: {
            open(screen) {
                this.screen = screen;

                $('#modal-delete').modal('show');
            },

            confirm() {
                this.$parent.confirmDelete(this.screen);
            }
        }
    });
</script>

<template id="screen-delete-modal-template">

<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                Are you sure you want to delete screen<b v-if="screen?.title">{{ ' ' + screen?.title }}</b>?
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary text-muted" data-dismiss="modal">Cancel</button>
                <button @click="confirm()" type="button" class="btn btn-danger"  data-dismiss="modal">Yes</button>
            </div>
        </div>
    </div>
</div>

</template>
@endverbatim
