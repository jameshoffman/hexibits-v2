<!--
    SCREEN PREVIEW
-->
@verbatim
<script>
    screensListApp.component('screen-preview', {
        template: '#screen-preview-template',

        props: ['screen', 'isEditing'],

        methods: {
            isType(type) {
                return this.screen.type == type;
            }
        }
    });
</script>

<template id="screen-preview-template">

    <div :style="{ 'background-color': this.screen.background_color }">
        <div v-if="isType('color')" class="w-100 h-100 border text-center" style="margin-bottom: 6px;">
            <div v-show="isEditing" class="font-weight-bold text-secondary bg-light">
                Select a source<br/>
                (or use as a color screen)
            </div>
        </div>

        <img v-if="isType('image')" :src="screen.source" style="object-fit: contain;" class="w-100 h-100 border" style="margin-bottom: 6px;" loading="lazy">

        <video v-if="isType('video')" class="w-100 h-100 border" controls>
            <source :src="screen.source" type="video/mp4" loading="lazy">
        </video>

        <iframe v-if="isType('url') || isType('html')" :src="screen.source" class="w-100 h-100 border" frameborder="0" scrolling="no" loading="lazy"></iframe>
    </div>

</template>
@endverbatim
