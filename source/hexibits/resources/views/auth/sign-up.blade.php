@extends('master')

@section('title', 'Sign up')

@section('container')
<div class="row mt-4">
    <div class="col text-center">
        <h1>hexibits</h1>
    </div>
</div>

<form method="POST">
    @csrf

    <div class="form-row">
        <div class="col-12 col-sm-6 offset-sm-3 col-lg-4 offset-lg-4">
            <div class="form-group text-muted">
                <h3>Create account</h3>

                @include('partials.errors')
            </div>
            <div class="form-group">
            <input type="email" class="form-control" id="email" name="email" placeholder="Email" required maxlength="150" value="{{ old('email') }}">
            </div>

            <div class="form-group">
                <input type="password" class="form-control" id="password" name="password" placeholder="Password" required maxlength="191">
            </div>

            <div class="form-group">
                <input type="password" class="form-control" id="passwordConfirmation" name="password_confirmation" placeholder="Password confirmation" required maxlength="191">
            </div>

            <div class="form-group">
                @include('partials.helpers.input-password',
                        ['id' => 'signUpToken', 'name' => 'sign_up_token', 'placeholder' => 'Sign up token', 'required' => 'required', 'disabled' => '', 'value' => old('sign_up_token')])
            </div>

            <div class="form-group text-center">
                <button type="submit" class="btn btn-success">Sign up</button>
            </div>

            <div class="form-group text-center mt-5">
                <p><i>Already have an account?</i> <a href="{{ url('login') }}">Log in</a></p>
            </div>
        </div>
    </div>
</form>
@endsection
