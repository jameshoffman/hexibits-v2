@extends('master')

@section('title', 'Login')

@section('container')

<div class="row mt-4">
    <div class="col text-center">
        <h1>hexibits</h1>
    </div>
</div>

<form action="{{ url('login') }}" method="POST">
    @csrf

    <div class="form-row">
        <div class="col-12 col-sm-6 offset-sm-3 col-lg-4 offset-lg-4">
            @include('partials.errors')

            <div class="form-group">
                <input type="email" class="form-control" id="email" placeholder="Email" required name="email" value="{{ old('email') }}">
            </div>

            <div class="form-group">
                <input type="password" class="form-control" id="password" placeholder="Password" required name="password">
            </div>

            <div class="form-group">
                <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" id="rememberMe" name="remember_me" {{ old('remember_me') ? 'checked' : ''}}>
                    <label class="custom-control-label" for="rememberMe">Remember me</label>
                </div>
            </div>

            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary">Log in</button>
            </div>

            <div class="form-group text-center mt-5">
                <p><i>Need an account?</i> <a class="text-success font-weight-bold" href="{{ url('sign-up') }}">Sign up</a></p>
            </div>
        </div>
    </div>
</form>
@endsection
