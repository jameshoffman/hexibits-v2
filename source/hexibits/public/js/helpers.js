function csrf() {
    return $('meta[name="csrf-token"]').attr('content');
}

function url(route) {
    return `${$('meta[name="base-url"]').attr('content')}/${route}`;
}

function sourceFormat(path) {
    return path.toLowerCase().startsWith('http') ? path : path.split('\\').pop().split('/').pop()
}
