<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScreensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('screens', function (Blueprint $table) {
            $table->id();
            $table->string('source', 500)->nullable();
            $table->string('title', 150)->nullable();
            $table->unsignedInteger('duration')->default(5);
            $table->string('background_color', 7)->default('#CCCCCC');
            $table->enum('type', ['color', 'image', 'video', 'url', 'html']);
            $table->unsignedInteger('position')->default(0);
            $table->unsignedBigInteger('display_id');
            $table->timestamps();

            // $table->foreign('display_id')
            //     ->references('id')
            //     ->on('displays')
            //     ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('screens');
    }
}
