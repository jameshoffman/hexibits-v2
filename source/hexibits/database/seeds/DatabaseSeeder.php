<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //
        // Users
        //

        $users = ['a', 'b', 'c'];

        foreach($users as $index => $user) {
            DB::table('users')->insert([
                'id' => ($index + 1),
                'email' => "$user@$user",
                'password' => Hash::make("$user"),
            ]);
        }

        //
        // Displays
        //

        // User a
        DB::table('displays')->insert([
            'name' => 'All data',
            'description' => 'With a description',
            'show_datetime' => true,
            'password' => '1234',
            'user_id' => 1
        ]);

        DB::table('displays')->insert([
            'name' => 'Only name',
            'description' => NULL,
            'show_datetime' => false,
            'password' => NULL,
            'user_id' => 1
        ]);


        DB::table('displays')->insert([
            'name' => 'Name and description',
            'description' => 'Some description text',
            'show_datetime' => false,
            'password' => NULL,
            'user_id' => 1
        ]);

        DB::table('displays')->insert([
            'name' => 'Only time',
            'description' => NULL,
            'show_datetime' => true,
            'password' => NULL,
            'user_id' => 1
        ]);

        DB::table('displays')->insert([
            'name' => 'Only password',
            'description' => NULL,
            'show_datetime' => false,
            'password' => '1234',
            'user_id' => 1
        ]);

        DB::table('displays')->insert([
            'name' => 'Mix and match',
            'description' => 'The description is here',
            'show_datetime' => true,
            'password' => NULL,
            'user_id' => 1
        ]);

        // User b
        DB::table('displays')->insert([
            'name' => 'Item',
            'description' => 'Description',
            'show_datetime' => false,
            'password' => NULL,
            'user_id' => 2
        ]);

        DB::table('displays')->insert([
            'name' => 'Another item',
            'description' => NULL,
            'show_datetime' => true,
            'password' => NULL,
            'user_id' => 2
        ]);

        DB::table('displays')->insert([
            'name' => 'Some item',
            'description' => 'a description',
            'show_datetime' => false,
            'password' => NULL,
            'user_id' => 2
        ]);

        DB::table('displays')->insert([
            'name' => 'The item',
            'description' => NULL,
            'show_datetime' => true,
            'password' => '1234',
            'user_id' => 2
        ]);

        // User c has no display

        // Set timestamps to all displays
        DB::table('displays')->update([
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        //
        // Screens
        //

        // Display: All Data
        DB::table('screens')->insert([
            'source' => null,
            'title' => '<3',
            'type' => 'color',
            'duration' => 10,
            'background_color' => '#FF0000',
            'position' => 3,
            'display_id' => 1
        ]);

        DB::table('screens')->insert([
            'source' => '/_demo/image.jpg',
            'title' => 'An image of the Earth',
            'type' => 'image',
            'duration' => 10,
            'background_color' => '#000000',
            'position' => 1,
            'display_id' => 1
        ]);

        DB::table('screens')->insert([
            'source' => '/_demo/video.mp4',
            'title' => NULL,
            'type' => 'video',
            'background_color' => '#0000FF',
            'position' => 2,
            'display_id' => 1
        ]);

        DB::table('screens')->insert([
            'source' => 'http://perdu.com/',
            'title' => 'A website!',
            'type' => 'url',
            'duration' => 2,
            'position' => 0,
            'display_id' => 1
        ]);

        DB::table('screens')->insert([
            'source' => '/_demo/html.html',
            'title' => 'An html file',
            'type' => 'html',
            'duration' => 7,
            'position' => 4,
            'display_id' => 1
        ]);

        DB::table('screens')->insert([
            'source' => '/_demo/html-folder/index.html',
            'title' => 'A folder',
            'type' => 'html',
            'duration' => 15,
            'position' => 5,
            'display_id' => 1
        ]);

        //Display: Only name
        DB::table('screens')->insert([
            'source' => '/_demo/image.jpg',
            'title' => 'An image of the Earth',
            'type' => 'image',
            'display_id' => 2
        ]);

        // Set timestamps to all screens
        DB::table('screens')->update([
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
