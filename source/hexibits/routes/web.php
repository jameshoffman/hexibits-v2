<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['guest']], function () {
    Route::get('login', 'Auth\LoginController@form')->name('login');
    Route::post('login', 'Auth\LoginController@login');

    Route::get('sign-up', 'Auth\SignUpController@form');
    Route::post('sign-up', 'Auth\SignUpController@store');
});

Route::group(['middleware' => ['auth']], function () {
    Route::post('logout', 'Auth\LogoutController');

    Route::redirect('/', 'displays');
    Route::get('displays', 'Displays\DisplaysController@index');
    Route::post('displays', 'Displays\DisplaysController@store');
    Route::get('displays/{display}', 'Displays\DisplaysController@show');
    Route::get('displays/{display}/form', 'Displays\DisplaysController@form');
    Route::patch('displays/{display}', 'Displays\DisplaysController@update');
    Route::delete('displays/{display}', 'Displays\DisplaysController@delete');

    Route::post('displays/{display}/screens', 'Screens\ScreensController@store');
    Route::patch('screens/{screen}', 'Screens\ScreensController@update');
    Route::delete('screens/{screen}', 'Screens\ScreensController@delete');
    Route::post('screens/{screen}/reorder', 'Screens\ScreensController@reorder');
});

Route::get('show/{display:name}', 'Presentation\PresentationController@show');
Route::get('show/{display:name}/load', 'Presentation\PresentationController@load');

Route::get('screens-resources/{filename}', 'Screens\ResourcesAccessController@show')->where('filename', '.*');;
