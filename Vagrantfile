# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
    config.vm.box = "hashicorp/bionic64"

    config.vm.network "forwarded_port", guest: 8000, host: 8765

    config.vm.provider "virtualbox" do |vb|
        vb.memory = "2048"
    end

    # Cannot share directly source/hexibits because permissions were attributed to root instead of vagrant user
    config.vm.synced_folder "./source", "/vagrant", type: "virtualbox"
  
    #
    # Provisioning
    #
  
    config.vm.provision "shell", inline: <<-'SHELL_PROVISON'
        # Upgrade needs to be completely non-interactive
        apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" upgrade

        # Utils, unzip is used by composer
        apt-get install -y curl vim unzip
        
        # Database
        apt-get install -y mariadb-server
    
        # mysql secure install
        mysql --user=root <<-'EOF_MYSQL'
            GRANT ALL PRIVILEGES on *.* to 'root'@'localhost' IDENTIFIED BY '@_passw0rd';
            DELETE FROM mysql.user WHERE User='';
            DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
            DROP DATABASE IF EXISTS test;
            DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
            CREATE DATABASE IF NOT EXISTS hexibits CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
            FLUSH PRIVILEGES;
EOF_MYSQL
  
        # PHP 7.2 for Laravel
        apt-get install -y php php-common php-cli php-intl php-mbstring php-mysql php-bcmath php-json php-tokenizer php-xml php-simplexml php-zip
        
        # Composer
        cd ~ && curl -sS https://getcomposer.org/installer -o composer-setup.php
        php composer-setup.php --install-dir=/usr/local/bin --filename=composer

        # Lavavel installer tool
        su - vagrant -c 'composer global require laravel/installer'
        su - vagrant -c "echo 'export PATH=$PATH:/home/vagrant/.config/composer/vendor/bin' >> /home/vagrant/.profile"
        su - vagrant -c 'cd /vagrant/hexibits && composer install -n'
        su - vagrant -c 'cd /vagrant/hexibits && cp .env.example .env'
        su - vagrant -c "sed -i 's+DB_DATABASE=laravel+DB_DATABASE=hexibits+g' /vagrant/hexibits/.env"
        su - vagrant -c "sed -i 's+DB_PASSWORD=+DB_PASSWORD=@_passw0rd+g' /vagrant/hexibits/.env"
        su - vagrant -c 'cd /vagrant/hexibits && php artisan key:generate'
        su - vagrant -c 'cd /vagrant/hexibits && php artisan migrate && php artisan db:seed'
SHELL_PROVISON
  
    #
    # UP trigger
    #
  
    config.trigger.after [:up, :reload] do |trigger|
        trigger.info = "Starting server!"
        trigger.run_remote = {inline: <<-'SHELL_TRIGGER'
            su - vagrant -c 'cd /vagrant/hexibits && composer install -n'
            su - vagrant -c 'cd /vagrant/hexibits && php artisan migrate:fresh --seed'
            su - vagrant -c 'php /vagrant/hexibits/artisan serve --host 0.0.0.0 &'
SHELL_TRIGGER
        }
    end
  end
  