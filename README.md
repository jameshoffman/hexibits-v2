# README - hexibits V2

- Download Virtualbox and Vagrant
- `vagrant up` to start VM
- `vagrant halt` to shutdown VM
- `vagrant reload` to reboot VM
- `vagrant ssh` to connect to the VM with SSH

## Getting started

- Initialize the VM with `vagrant up && vagrant halt`
- Run `vagrant up && vagrant ssh`
  - `cd /vagrant/hexibits` inside the VM to reach the app's directory
  - run commands here (artisan, etc.)
  - When you're done, `exit` SSH then `vagrant halt`
- Open <http://localhost:8765/> in your browser
  - Default token to sign up is *the_token*
  - Default users are seeded: *a@a*:*a*, *b@b*:*c* and *c@c*:*c*

## Deployment

- Initialize Laravel from `source` directory
```bash
cp .env.example .env

vim .env
# set correct SIGN_UP_TOKEN, DEBUG to false, APP_URL, DB_DATABASE, DB_PASSWORD

php artisan key:generate
php artisan migrate
php artisan storage:link
```
- Set upload_max_filesize and post_max_size in PHP config, ex:

```
upload_max_filesize = 64M
post_max_size = 70M
```